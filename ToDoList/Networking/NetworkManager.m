//
//  NetworkManager.m
//  ToDoList
//
//  Created by Salma Khattab on 16/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "NetworkManager.h"
#import <AFImageDownloader.h>
//#import <UIImageView+AFNetworking.h>

static NSString *baseURLPath = @"https://adsapp.net/wp-json/";
static NSString *userName = @"eng.salma.khattab@gmail.com";
static NSString *password = @"Salma@12345";

@implementation NetworkManager

static NetworkManager *sharedManager = nil;

+ (NetworkManager*)sharedManager {
    
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        sharedManager = [[NetworkManager alloc] init];
    });
    return sharedManager;
}

- (id)init {
    
    if ((self = [super init])) {
        
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.HTTPMaximumConnectionsPerHost = 5;
        self.networkingManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURLPath] sessionConfiguration:config];
        self.networkingManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        self.networkingManager.responseSerializer.acceptableContentTypes = [self.networkingManager.responseSerializer.acceptableContentTypes setByAddingObjectsFromArray:@[@"application/json", @"image/png", @"image/jpeg"]];
        self.networkingManager.securityPolicy = [AFSecurityPolicy defaultPolicy];
        self.imageCache = [[NSCache alloc] init];
    }
    return self;
}

- (void)authenticateWithEmail:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
   
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:userName forKey:@"username"];
    [params setObject:password forKey:@"password"];
    [self.networkingManager POST:@"userdetails/login-api" parameters:params headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if (success != nil) {
            
            NSDictionary *infoDic = responseObject;
            if(infoDic[@"status"]) {
                
                NSInteger status = [infoDic[@"status"] integerValue];
                if(status == 1) {
                                    
                    [[NSUserDefaults standardUserDefaults] setObject:infoDic[@"details"][@"user_pass"] forKey:@"token"];
                    [[NSUserDefaults standardUserDefaults] setObject:infoDic[@"details"][@"ID"] forKey:@"userID"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    success(responseObject);
                } else {
                    
                    failure(infoDic[@"message"], status);
                }
            }
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSString *errorMessage = [self getError:error];
        if (failure != nil) {
            
            failure(errorMessage, ((NSHTTPURLResponse*)operation.response).statusCode);
        }
    }];
}

- (void)setRequestHeaders {
    
    [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    
    [self.networkingManager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"] forHTTPHeaderField:@"token"];
    [self.networkingManager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] forHTTPHeaderField:@"userid"];
}

- (void)getCategoryList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {

  //  [self setRequestHeaders];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"token"]  forKey:@"token"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"]  forKey:@"userid"];
    [self.networkingManager GET:@"status/getting-userstatus-api" parameters:nil headers:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        if (success != nil) {
            
            NSDictionary *infoDic = responseObject;
            if(infoDic[@"status"]) {
                
                NSInteger status = [infoDic[@"status"] integerValue];
                if(status == 1) {
                
                    NSLog(@"icons retrieved");
                    success(responseObject);
                } else {
                    
                    failure(infoDic[@"message"], status);
                }
            }
        }
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSString *errorMessage = [self getError:error];
        if (failure != nil) {
            
            failure(errorMessage, ((NSHTTPURLResponse*)operation.response).statusCode);
        }
    }];
}

- (void)loadImageFromURL:(NSString*)urlKey success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure {
    
    UIImage *img = [self getCachedImageForKey:urlKey];
    if(img){
        
        success(img);
    } else {
        
        NSURL *URL = [NSURL URLWithString:urlKey];
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        AFImageDownloader *downloader = [AFImageDownloader defaultInstance];
        [downloader downloadImageForURLRequest:request success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {

            NSLog(@"Succes download image");
            [self cacheImage:responseObject forKey:urlKey];
            success(responseObject);

        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {

            NSLog(@"Error download image");
        }];
    }
}

- (NSString*)getError:(NSError*)error {
    
    if (error != nil) {
        
        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        if (responseObject != nil && [responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"message"] != nil && [[responseObject objectForKey:@"message"] length] > 0) {
            return [responseObject objectForKey:@"message"];
        }
    }
    return @"Server Error. Please try again later";
}

#pragma mark - caching

- (void)cacheImage:(UIImage*)image forKey:(NSString*)key {
    [self.imageCache setObject:image forKey:key];
}

- (UIImage*)getCachedImageForKey:(NSString*)key {
    return [self.imageCache objectForKey:key];
}

@end
