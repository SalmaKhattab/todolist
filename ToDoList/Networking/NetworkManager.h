//
//  NetworkManager.h
//  ToDoList
//
//  Created by Salma Khattab on 16/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^NetworkManagerSuccess)(id responseObject);
typedef void (^NetworkManagerFailure)(NSString *failureReason, NSInteger statusCode);

@interface NetworkManager : NSObject

@property (nonatomic, strong) AFHTTPSessionManager *networkingManager;
@property (nonatomic, strong) NSCache *imageCache;

+ (id)sharedManager;
- (void)getCategoryList:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)authenticateWithEmail:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;
- (void)loadImageFromURL:(NSString*)urlKey success:(NetworkManagerSuccess)success failure:(NetworkManagerFailure)failure;

@end

NS_ASSUME_NONNULL_END
