//
//  List.h
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "Item.h"

NS_ASSUME_NONNULL_BEGIN

RLM_ARRAY_TYPE(List)

@interface List : RLMObject

@property (nonatomic) NSString *listID;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *iconURL;
@property (nonatomic) NSInteger count;
@property (nonatomic) NSInteger checkedCount;
@property (nonatomic, readonly) NSInteger uncheckedCount;
@property RLMArray<Item *><Item> *items;

- (void)addObject;
- (void)deleteObject;
- (void)addNewItem:(Item*)newItem;
+ (RLMResults*)fetchAll;
+ (RLMResults*)Query:(NSString*)queryString;
+ (void)updateObject:(NSDictionary*)newValue;

@end

NS_ASSUME_NONNULL_END
