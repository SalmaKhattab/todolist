//
//  RealmHelper.h
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <Realm.h>

NS_ASSUME_NONNULL_BEGIN

@interface RealmHelper : NSObject

@property RLMRealm* realm;

+ (RealmHelper*)sharedHelper;
- (void)addObject:(id)obj;
- (void)deleteObject:(id)obj;

@end

NS_ASSUME_NONNULL_END
