//
//  RealmHelper.m
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "RealmHelper.h"

@implementation RealmHelper


static RealmHelper *sharedHelper = nil;

+ (RealmHelper*)sharedHelper {
    
    static dispatch_once_t once;
    dispatch_once(&once, ^
    {
        sharedHelper = [[RealmHelper alloc] init];
    });
    return sharedHelper;
}

- (id)init {
    
    if ((self = [super init])) {
        
        _realm = [RLMRealm defaultRealm];
    }
    return self;
}

- (void)addObject:(id)obj {
    
    [self.realm transactionWithBlock:^{
        [self.realm addObject:obj];
    }];
}

- (void)deleteObject:(id)obj {
    
    [self.realm beginWriteTransaction];
    [self.realm deleteObject:obj];
    [self.realm commitWriteTransaction];
}

@end
