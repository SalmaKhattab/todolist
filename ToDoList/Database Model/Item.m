//
//  Item.m
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "Item.h"

static NSString *PostNotification = @"ItemDataRetrived";

@implementation Item


+ (NSString *)PostNotification{
    
    return PostNotification;
}

+ (NSArray *)requiredProperties {
    
    return @[@"name", @"checked"];
}

+ (NSString *)primaryKey {
    
    return @"itemID";
}

+ (NSDictionary *)defaultPropertyValues {
    
    return @{@"name": @"", @"checked":@NO, @"isReminderOn": @NO, @"note": @"", @"repeatOption": @0};
}

+ (void)updateItem:(NSDictionary*)newValues {
    
    [[RealmHelper sharedHelper].realm beginWriteTransaction];
    [Item createOrUpdateModifiedInRealm:[RealmHelper sharedHelper].realm withValue:newValues];
    [[RealmHelper sharedHelper].realm commitWriteTransaction];
}

- (void)deleteItem {

    [[RealmHelper sharedHelper] deleteObject:self];
}

@end
