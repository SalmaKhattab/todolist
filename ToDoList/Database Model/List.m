//
//  List.m
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "List.h"

static NSString *PostNotification = @"ListDataRetrived";

@implementation List

+ (NSString *)PostNotification{
    
    return PostNotification;
}

+ (NSArray *)ignoredProperties {
    
    return @[@"count", @"uncheckedCount", @"checkedCount"];
}

+ (NSArray *)requiredProperties {
    
    return @[@"title", @"iconURL"];
}

+ (NSString *)primaryKey {
    
    return @"listID";
}

+ (NSDictionary *)defaultPropertyValues {
    
    return @{@"count" : @0, @"title": @"", };
}

- (NSInteger)checkedCount {
    
    RLMResults<Item *> *items = [Item objectsWhere:@"checked == 1"];
    return items.count;
}

- (NSInteger)uncheckedCount {
    
    return self.count - self.checkedCount;
}

- (NSInteger)count {
    
    return self.items.count;
}

- (void)addObject {
    
    [[RealmHelper sharedHelper] addObject:self];
}

- (void)addNewItem:(Item*)newItem {
    
    [[RealmHelper sharedHelper].realm beginWriteTransaction];
    [self.items addObject:newItem];
    [[RealmHelper sharedHelper].realm commitWriteTransaction];
}

- (void)deleteObject {
    
    [[RealmHelper sharedHelper] deleteObject:self];
}

+ (RLMResults*)fetchAll {
    
    RLMResults<List *> *lists = [List allObjects];
    return lists;
}

+ (RLMResults*)Query:(NSString*)queryString {
    
    RLMResults *queryList = [List objectsWhere:@"ANY items.name contains[cd] %@ or title contains[cd] %@",queryString, queryString ];
    return queryList;
}

+ (void)updateObject:(NSDictionary*)newValues {

    [[RealmHelper sharedHelper].realm beginWriteTransaction];
    [List createOrUpdateModifiedInRealm:[RealmHelper sharedHelper].realm withValue:newValues];
    [[RealmHelper sharedHelper].realm commitWriteTransaction];
}

//RLMResults<Person *> *ownersByDogAge = [dogOwners sortedResultsUsingKeyPath:@"dog.age" ascending:YES];

@end
