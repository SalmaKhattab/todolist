//
//  Item.h
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <Realm.h>
#import "RealmHelper.h"

NS_ASSUME_NONNULL_BEGIN

RLM_ARRAY_TYPE(Item)

@interface Item : RLMObject

@property (nonatomic) NSString *itemID;
@property (nonatomic) NSString *name;
@property (nonatomic) BOOL checked;
@property (nonatomic) NSDate *reminderDate;
@property (nonatomic) BOOL isReminderOn;
@property (nonatomic) NSString *note;
@property (nonatomic) NSInteger repeatOption;
@property (nonatomic) NSString *notificationId;
@property (nonatomic) NSString *repeatNotificationId;

+ (void)updateItem:(NSDictionary*)newValues;
- (void)deleteItem;

@end

NS_ASSUME_NONNULL_END
