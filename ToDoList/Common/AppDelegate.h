//
//  AppDelegate.h
//  ToDoList
//
//  Created by Salma Khattab on 14/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>

@import UserNotifications;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

- (void)loginForIconList;

@end

