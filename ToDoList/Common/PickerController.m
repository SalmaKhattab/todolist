//
//  PickerController.m
//  ToDoList
//
//  Created by Salma Khattab on 19/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "PickerController.h"

@implementation PickerController {
    
    NSArray *pickerData;
    UIView *pickerView;
}

@dynamic delegate;

- (instancetype)initWithOption:(id)option {
    
    if ((self = [[PickerController alloc] init])) {
        
        if ([option isKindOfClass:[NSArray class]]) {
            
            NSArray *staticList = option;
            pickerData = staticList;
            pickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
            ((UIPickerView*)pickerView).dataSource = self;
            ((UIPickerView*)pickerView).delegate = self;
        } else if ([option isKindOfClass:[NSDate class]]){
            
            NSDate *date = option;
            pickerView = [[UIDatePicker alloc] initWithFrame:CGRectZero];
            ((UIDatePicker*) pickerView).datePickerMode = UIDatePickerModeDateAndTime;
            if (date) {
                
                ((UIDatePicker*) pickerView).date = date;
            }
        }
        [self addInputAccessoryView];
    }
    return self;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return pickerData.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectedItem = pickerData[row];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return pickerData[row][@"Title"];
}

- (void)addInputAccessoryView {
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done",nil) style:UIBarButtonItemStyleDone target:self action:@selector(donePickerView)];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel",nil) style:UIBarButtonItemStyleDone target:self action:@selector(cancelPickerView)];
    [doneButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateNormal];
    [cancelButton setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]} forState:UIControlStateNormal];
    [doneButton setTintColor:[UIColor systemOrangeColor]];
    [doneButton setTintColor:[UIColor systemOrangeColor]];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    NSArray *toolbarItems = @[cancelButton, flexibleSpace, doneButton];
    [toolBar setItems:toolbarItems];
    [self setInputAccessoryView:toolBar];
    [self setInputView:pickerView];
}

- (void)cancelPickerView {
    
    [self resignFirstResponder];
}

- (void)donePickerView {
    
    [self resignFirstResponder];
    if ([pickerView isKindOfClass:[UIDatePicker class]]) {
        
        self.selectedItem = [(UIDatePicker *)pickerView date];
    }
    [[self delegate] pickerSelectionController:self didSelectItem:self.selectedItem];
}

@end
