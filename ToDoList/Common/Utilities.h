//
//  Utilities.h
//  ToDoList
//
//  Created by Salma Khattab on 17/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utilities : NSObject

+ (UIAlertController *)displayMyAlertError:(NSString*)errorMessage;
+ (BOOL)setCachingList:(NSString*)path cachingList:(NSArray*)list;
+ (NSArray*)getCachingList:(NSString*)path;
+ (BOOL)checkCachingList:(NSString*)path;
+ (void)clearCachingList;
+ (NSString *)getIconListPath;
+ (NSArray *)getRepeatList;
+ (void)loginForIconListWithCompletion:(void (^__nullable)(void))completionHandler;

@end

NS_ASSUME_NONNULL_END
