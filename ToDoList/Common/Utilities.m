//
//  Utilities.m
//  ToDoList
//
//  Created by Salma Khattab on 17/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "Utilities.h"
#import "NetworkManager.h"

@implementation Utilities

+ (UIAlertController *)displayMyAlertError:(NSString *)errorMessage {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) { }];
    [alert addAction:okButton];
    return alert;
}

+ (BOOL)setCachingList:(NSString*)name cachingList:(NSArray*)list {
    
    NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@.plist",name];
    return [list writeToFile:path atomically:YES];
}

+ (NSArray*)getCachingList:(NSString*)name {
    
    NSString *path= [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@.plist",name];
    return [NSArray arrayWithContentsOfFile:path];
}

+ (BOOL)checkCachingList:(NSString*)name {
    
    NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@.plist",name];
    NSArray *list = [NSArray arrayWithContentsOfFile:path];
    return list.count > 0;
}

+ (void)clearCachingList {
    
    NSString *documents = [NSHomeDirectory() stringByAppendingString:@"/Documents"];
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documents error:NULL];
    for (NSString *file in files) {
        
        [[NSFileManager defaultManager] removeItemAtPath:[documents stringByAppendingFormat:@"/%@", file] error:NULL];
    }
}

+ (NSString *)getIconListPath {
    
    NSDictionary *dataDict = [Utilities getPlistData:@"Info"];
    return dataDict[@"IconListPath"];
}

+ (NSArray *)getRepeatList {
    
    NSDictionary *dataDict = [Utilities getPlistData:@"Info"];
    return dataDict[@"RepeatList"];
}

+ (NSDictionary *)getPlistData:(NSString *)fileName {

    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath;
    rootPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    plistPath = [rootPath stringByAppendingPathComponent:[fileName stringByAppendingString:@".plist"]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {

        plistPath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"plist"];
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *dataDict = (NSDictionary *) [NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:nil];
    return dataDict;
}

+ (void)loginForIconListWithCompletion:(void (^__nullable)(void))completionHandler{
    
    [[NetworkManager sharedManager] authenticateWithEmail:^(id responseObject) {
        
        [[NetworkManager sharedManager] getCategoryList:^(id responseObject) {
            
            dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                
                NSDictionary *dic = responseObject;
                NSArray *list = dic[@"details"];
                [Utilities setCachingList:[Utilities getIconListPath] cachingList:list];
                if(completionHandler)
                    completionHandler();
            });
        } failure:^(NSString *failureReason, NSInteger statusCode) {
            
            NSLog(@"%@", failureReason);
        }];
    } failure:^(NSString *failureReason, NSInteger statusCode) {
        
        NSLog(@"%@", failureReason);
    }];
}

@end
