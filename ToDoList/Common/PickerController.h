//
//  PickerController.h
//  ToDoList
//
//  Created by Salma Khattab on 19/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class PickerController;

@protocol PickerControllerDelegate

- (void)pickerSelectionController:(PickerController *)pickerController didSelectItem:(id)item;
    
@end

@interface PickerController : UITextField <UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

@property (nonatomic, weak) id<PickerControllerDelegate, UITextFieldDelegate> delegate;
@property (nonatomic, strong)  id selectedItem;

- (instancetype)initWithOption:(id)option;

@end

NS_ASSUME_NONNULL_END




