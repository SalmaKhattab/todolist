//
//  IconCollectionViewCell.m
//  ToDoList
//
//  Created by Salma Khattab on 18/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "IconCollectionViewCell.h"
#import "NetworkManager.h"

@implementation IconCollectionViewCell

- (void)awakeFromNib {

    [super awakeFromNib];
}

- (void)setDataSource:(NSMutableDictionary *)dataSource {
    
    _dataSource = dataSource;
    self.layer.borderColor = [[UIColor blackColor] CGColor];
    self.layer.borderWidth = _selectedIndex ? 2.0 : 0.0;
    self.iconImageView.image = [UIImage imageNamed:@"PlaceHolder"];
    NSString *imageURL = dataSource[@"status_image"];
    [[NetworkManager sharedManager] loadImageFromURL:imageURL success:^(id responseObject) {
        
        UIImage *img = responseObject;
        self.iconImageView.image = img;
        [self setNeedsLayout];
    } failure:^(NSString *failureReason, NSInteger statusCode) {
        
        self.iconImageView.image = [UIImage imageNamed:@"CorruptionIcon"];
    }];
}

@end
