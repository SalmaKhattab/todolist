//
//  ReminderViewController.m
//  ToDoList
//
//  Created by Salma Khattab on 19/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "ReminderViewController.h"
#import "Utilities.h"
@import UserNotifications;

@interface ReminderViewController () 

@property(nonatomic, strong) PickerController *dateTextField;
@property(nonatomic, strong) PickerController *repeatTextField;

@end

@implementation ReminderViewController {
    
    NSArray *selectionControllers;
    NSInteger tableRows;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.title = @"Reminder";
    [self initselectionControllers];
    
    self.reminderSwitch.on = self.selectedElement.isReminderOn;
    self.noteTextView.text = self.selectedElement.note;
    tableRows = self.reminderSwitch.on ? 2 : 0;
    [self.reminderTableView reloadData];
}

- (void)initselectionControllers {
    
    NSArray *repeatList = [Utilities getRepeatList];
    [self setValue:[[PickerController alloc] initWithOption:repeatList]  forKey:@"repeatTextField"];
    PickerController *repeatSelectionController = [self valueForKey:@"repeatTextField"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Value == %@", @(self.selectedElement.repeatOption)];
    NSArray *filteredArray = [repeatList filteredArrayUsingPredicate:predicate];
    repeatSelectionController.selectedItem = (filteredArray.count > 0) ? filteredArray[0] : repeatList[0];
    repeatSelectionController.delegate = self;
    
    [self setValue:[[PickerController alloc] initWithOption:[NSDate date]]  forKey:@"dateTextField"];
    PickerController *dateSelectionController = [self valueForKey:@"dateTextField"];
    dateSelectionController.delegate = self;
    dateSelectionController.selectedItem = (self.selectedElement.reminderDate) ? self.selectedElement.reminderDate : [NSDate date];
    
    selectionControllers = @[self.dateTextField, self.repeatTextField];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return tableRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reminderCell" forIndexPath:indexPath];
    [self configureCell:cell atIndex:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndex:(NSIndexPath *)indexPath {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm a"];
    NSString *dateString = [dateFormatter stringFromDate:((NSDate *)_dateTextField.selectedItem)];
    NSArray *titles = @[@"Date", @"Repeat"];
    NSArray *values = @[dateString, _repeatTextField.selectedItem[@"Title"]];
    cell.textLabel.text = titles[indexPath.row];
    cell.detailTextLabel.text = values[indexPath.row];
    [cell.contentView addSubview:selectionControllers[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [selectionControllers[indexPath.row] becomeFirstResponder];
}

#pragma mark - Navigation

//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//
//    if([segue.identifier isEqualToString:@"showListDetailsSegue"]) {
//
//        ListDetailedTableViewController *vc = segue.destinationViewController;
//        vc.myList = sender;
//    } else if([segue.identifier isEqualToString:@"addNewListSegue"]) {
//
//        AddNewListViewController *vc = segue.destinationViewController;
//      }
//  }

#pragma mark - TextView Delegete

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    [textView resignFirstResponder];
    return NO;
}

#pragma mark - PickerController Delegete

- (void)pickerSelectionController:(nonnull PickerController *)pickerController didSelectItem:(nonnull id)item {
    
    [self.reminderTableView reloadData];
}

#pragma mark - Actions

- (IBAction)reminderSwitchValueChanged:(id)sender {
    
    tableRows = self.reminderSwitch.on ? 2 : 0;
    [self.reminderTableView reloadData];
}

- (IBAction)saveReminderClicked:(id)sender {
    
    NSString *notificationId = [[NSUUID UUID] UUIDString];
    NSString *repeatNotificationId = [[NSUUID UUID] UUIDString];
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    if(self.selectedElement.notificationId) {
        
        [center removePendingNotificationRequestsWithIdentifiers:@[self.selectedElement.notificationId]];
    }
    if(self.selectedElement.repeatNotificationId) {
        
        [center removePendingNotificationRequestsWithIdentifiers:@[self.selectedElement.repeatNotificationId]];
    }
    NSDictionary *dic = self.reminderSwitch.on ? @{@"itemID":_selectedElement.itemID, @"note": self.noteTextView.text, @"isReminderOn":@(YES), @"reminderDate": ((NSDate *)_dateTextField.selectedItem), @"repeatOption": _repeatTextField.selectedItem[@"Value"], @"notificationId" : notificationId, @"repeatNotificationId" : repeatNotificationId} : @{@"itemID":_selectedElement.itemID, @"note": self.noteTextView.text, @"isReminderOn":@(NO)};
    [Item updateItem:dic];
    
    if(self.reminderSwitch.on) {
        
        NSDate *reminderDate = ((NSDate *)_dateTextField.selectedItem);
        NSTimeInterval reminderTimeInterval = [reminderDate timeIntervalSinceDate:[NSDate date]];
        UNMutableNotificationContent *objNotificationContent = [[UNMutableNotificationContent alloc] init];
        objNotificationContent.title = [NSString localizedUserNotificationStringForKey:@"Notification!" arguments:nil];
        objNotificationContent.body = [NSString localizedUserNotificationStringForKey:self.selectedElement.name arguments:nil];
        objNotificationContent.sound = [UNNotificationSound defaultSound];
        objNotificationContent.badge = @([[UIApplication sharedApplication] applicationIconBadgeNumber] + 1);
        
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:reminderTimeInterval repeats:NO];
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:notificationId content:objNotificationContent trigger:trigger];
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            
            if (!error) {
                
                NSLog(@"Local Notification succeeded");
            } else {
                
                NSLog(@"Local Notification failed");
            }
        }];
        
        NSInteger repeatOption = [_repeatTextField.selectedItem[@"Value"] integerValue];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:reminderDate];
        NSDateComponents *date;
        switch (repeatOption) {
            case 1:{
                
                date = [[NSDateComponents alloc] init];
                date.hour = [components hour];
                date.minute = [components minute];
            }
                break;
            case 2: {
                
                date = [[NSDateComponents alloc] init];
                date.hour = [components hour];
                date.minute = [components minute];
                date.weekday = [components weekday];
            }
                break;
            case 3:{
                
                date = [[NSDateComponents alloc] init];
                date.hour = [components hour];
                date.minute = [components minute];
                date.day = [components day];
            }
                break;
            case 4:{
                
                date = [[NSDateComponents alloc] init];
                date.hour = [components hour];
                date.minute = [components minute];
                date.month = [components month];
            }
                break;
            default:
                break;
        }
        if(date) {
            
            UNCalendarNotificationTrigger* repeatTrigger = [UNCalendarNotificationTrigger
                                                            triggerWithDateMatchingComponents:date repeats:YES];
            UNNotificationRequest *repeatRequest = [UNNotificationRequest requestWithIdentifier:notificationId content:objNotificationContent trigger:repeatTrigger];
            [center addNotificationRequest:repeatRequest withCompletionHandler:^(NSError * _Nullable error) {
                
                if (!error) {
                    
                    NSLog(@"Local Notification succeeded");
                } else {
                    
                    NSLog(@"Local Notification failed");
                }
            }];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end

