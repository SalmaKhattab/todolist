//
//  MainTableViewCell.m
//  ToDoList
//
//  Created by Salma Khattab on 14/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "MainTableViewCell.h"
#import "NetworkManager.h"

@implementation MainTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setDataSource:(List *)dataSource {
    
    _dataSource = dataSource;
    self.mainLabel.text = dataSource.title;
    self.subLabel.text = [NSString stringWithFormat:@"%li / %li item",(long)dataSource.checkedCount, (long)dataSource.count];
    self.completedListHeightConstraint.constant = (dataSource.checkedCount == dataSource.count) ? (dataSource.count != 0 ) ? 40 : 0 : 0;
    self.iconImageView.image = [UIImage imageNamed:@"PlaceHolder"];
    NSString *imageURL = dataSource.iconURL;
    [[NetworkManager sharedManager] loadImageFromURL:imageURL success:^(id responseObject) {
           
           UIImage *img = responseObject;
           self.iconImageView.image = img;
           [self setNeedsLayout];
    } failure:^(NSString *failureReason, NSInteger statusCode) {           

        self.iconImageView.image = [UIImage imageNamed:@"CorruptionIcon"];
    }];
}

@end
