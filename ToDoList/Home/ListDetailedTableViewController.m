//
//  ListDetailedTableViewController.m
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "ListDetailedTableViewController.h"
#import "ItemTableViewCell.h"
#import "ReminderViewController.h"

@interface ListDetailedTableViewController ()

@end

@implementation ListDetailedTableViewController {
    
    RLMArray *items;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    items = _myList.items;
    self.navigationItem.title = _myList.title;
    [self.tableView registerNib:[UINib nibWithNibName:@"ItemTableViewCell" bundle:nil] forCellReuseIdentifier:@"ItemCell"];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (items.count > 0) {
        
        self.tableView.backgroundView = nil;
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"you have no items, Yet!";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:20];
        [messageLabel sizeToFit];
        self.tableView.backgroundView = messageLabel;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell" forIndexPath:indexPath];
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:250.0/255.0 blue:242.0/255.0 alpha:1.0];
    else
        cell.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    cell.dataSource = items[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Item *selectedItem = items[indexPath.row];
    NSDictionary *dic = @{@"itemID":selectedItem.itemID, @"checked":@(!selectedItem.checked)};
    [Item updateItem:dic];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Reminder" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        [self performSegueWithIdentifier:@"showReminderSegue" sender:self->items[indexPath.row]];
    }];
    editAction.backgroundColor = [UIColor orangeColor];
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete List" message:@"Are you sure you want to delete this list?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {

            Item *selectedItem = self->items[indexPath.row];
            [selectedItem deleteItem];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
        UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {
        }];
        [alert addAction:okButton];
        [alert addAction:cancelButton];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction,editAction];
}

#pragma mark - Navigation


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"showReminderSegue"]) {
        
        ReminderViewController *vc = segue.destinationViewController;
        vc.selectedElement = sender;
    }
}

#pragma mark - Actions

- (IBAction)addNewItemClicked:(id)sender {
    
    [self showInputAlert];
}

-(void)showInputAlert {
    
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Add new item" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertVC addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
        textField.placeholder = @"Insert title";
        textField.textColor = [UIColor blackColor];
        textField.textAlignment = NSTextAlignmentCenter;
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    }];
    
    UIAlertAction *addAction=[UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if(![alertVC.textFields[0].text isEqualToString:@""]) {
            
            Item *newItem = [[Item alloc] init];
            newItem.itemID = [[NSUUID UUID] UUIDString];
            newItem.name = alertVC.textFields[0].text;
            [self->_myList addNewItem:newItem];
            self->items = self->_myList.items;
            [self.tableView reloadData];
        }
    }];
    
    UIAlertAction *addCreateAction=[UIAlertAction actionWithTitle:@"Add & Create New" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        if(![alertVC.textFields[0].text isEqualToString:@""]) {
            
            Item *newItem = [[Item alloc] init];
            newItem.itemID = [[NSUUID UUID] UUIDString];
            newItem.name = alertVC.textFields[0].text;
            [self->_myList addNewItem:newItem];
            self->items = self->_myList.items;
            [self.tableView reloadData];
            [self showInputAlert];
        }
    }];
    
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertVC addAction:addAction];
    [alertVC addAction:addCreateAction];
    [alertVC addAction:cancelAction];
    [self presentViewController:alertVC animated:true completion:nil];
}

@end
