//
//  ItemTableViewCell.m
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "ItemTableViewCell.h"

@implementation ItemTableViewCell

- (void)awakeFromNib {
    
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setDataSource:(Item *)dataSource {
    
    _dataSource =dataSource;
    self.iconImageView.image = dataSource.checked ? [UIImage imageNamed:@"CheckedIcon"] : [UIImage imageNamed:@"UncheckedIcon"];
    self.reminderImageViewHeightConstraint.constant = dataSource.isReminderOn ? 20 : 0;
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:dataSource.name];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName value:dataSource.checked ? @1 : @0 range:NSMakeRange(0, [attributeString length])];
    self.mainLabel.attributedText = attributeString;
}

@end
