//
//  AddNewListViewController.m
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "AddNewListViewController.h"
#import "Utilities.h"
#import "IconCollectionViewCell.h"

@interface AddNewListViewController ()

@end

@implementation AddNewListViewController {
    
    NSArray *iconsList;
    NSInteger selectedIndex;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    iconsList = [NSArray array];
    selectedIndex = -1;
    if([Utilities checkCachingList:[Utilities getIconListPath]]) {
        
        iconsList = [Utilities getCachingList:[Utilities getIconListPath]];
        [self.iconCollectionView reloadData];
    } else {
        
        [Utilities loginForIconListWithCompletion:^{
            
            self->iconsList = [Utilities getCachingList:[Utilities getIconListPath]];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                
                [self.iconCollectionView reloadData];
            });
        }];
    }
    if(_myList) {
        
        self.navigationItem.title = @"Edit List";
        NSDictionary *dic = [[iconsList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"status_image == %@", _myList.iconURL]] firstObject];
        selectedIndex = [iconsList indexOfObject:dic];
        self.titleTextField.text = _myList.title;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return iconsList.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellIdentifier = @"iconCell";
    IconCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.selectedIndex = indexPath.row == selectedIndex;
    cell.dataSource = iconsList[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.titleTextField resignFirstResponder];
    selectedIndex = indexPath.row;
    [self.iconCollectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger paddingSpace = 10 * 3;
    float availableWidth = self.view.frame.size.width - paddingSpace - 24;
    float widthPerItem = availableWidth / 3;
    return CGSizeMake(widthPerItem, widthPerItem);
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
  return UIEdgeInsetsMake(5,5,5,5); // top, left, bottom, right
}

#pragma mark -  TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

#pragma mark -  Actions

- (IBAction)saveButtonClicked:(id)sender {
    
    [self.titleTextField resignFirstResponder];
    if([self.titleTextField.text isEqualToString:@""]) {
        
        [self presentViewController:[Utilities displayMyAlertError:@"Please enter title"] animated:YES completion:nil];
    } else if(selectedIndex == -1){
        
         [self presentViewController:[Utilities displayMyAlertError:@"Please select icon"] animated:YES completion:nil];
    } else {
        
        if(!_myList) {
            
            List *newList = [[List alloc] init];
            newList.listID = [[NSUUID UUID] UUIDString];
            newList.title = self.titleTextField.text;
            newList.iconURL = iconsList[selectedIndex][@"status_image"];
            [newList addObject];
        } else {
            
            NSDictionary *dic = @{@"listID":_myList.listID, @"title": self.titleTextField.text, @"iconURL":iconsList[selectedIndex][@"status_image"]};
            [List updateObject:dic];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
