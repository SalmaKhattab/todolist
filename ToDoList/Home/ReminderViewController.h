//
//  ReminderViewController.h
//  ToDoList
//
//  Created by Salma Khattab on 19/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"
#import "PickerController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ReminderViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, PickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *noteTextView;
@property (weak, nonatomic) IBOutlet UISwitch *reminderSwitch;
@property (weak, nonatomic) IBOutlet UITableView *reminderTableView;
@property (nonatomic) Item *selectedElement;

@end

NS_ASSUME_NONNULL_END
