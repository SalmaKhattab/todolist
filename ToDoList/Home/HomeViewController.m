//
//  HomeViewController.m
//  ToDoList
//
//  Created by Salma Khattab on 14/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "HomeViewController.h"
#import "MainTableViewCell.h"
#import "List.h"
#import "ListDetailedTableViewController.h"
#import "AddNewListViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController {
    
    RLMResults *myLists;
    List *editedList;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.title = @"Home";
    [self.tableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
}

- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    editedList = nil;
    myLists = [List fetchAll];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (myLists.count > 0) {
        
        self.tableView.backgroundView = nil;
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"Create your own list right now!";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:20];
        [messageLabel sizeToFit];
        self.tableView.backgroundView = messageLabel;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return myLists.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath:indexPath];
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:250.0/255.0 blue:242.0/255.0 alpha:1.0];
    else
        cell.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    cell.dataSource = myLists[indexPath.row];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        self->editedList = self->myLists[indexPath.row];
        [self performSegueWithIdentifier:@"addNewListSegue" sender:self->editedList];
        
    }];
    editAction.backgroundColor = [UIColor orangeColor];
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Delete Item" message:@"Are you sure you want to delete this item?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {

            List *selectedList = self->myLists[indexPath.row];
            [selectedList deleteObject];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }];
        UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *actionHandler) {
        }];
        [alert addAction:okButton];
        [alert addAction:cancelButton];
        [self presentViewController:alert animated:YES completion:nil];
    }];
    deleteAction.backgroundColor = [UIColor redColor];
    return @[deleteAction,editAction];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"showListDetailsSegue" sender:myLists[indexPath.row]];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"showListDetailsSegue"]) {
        
        ListDetailedTableViewController *vc = segue.destinationViewController;
        vc.myList = sender;
    } else if([segue.identifier isEqualToString:@"addNewListSegue"]) {
        
        AddNewListViewController *vc = segue.destinationViewController;
        vc.myList = self->editedList ? sender : nil;
    }
}

@end
