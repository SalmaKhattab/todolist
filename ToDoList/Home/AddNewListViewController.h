//
//  AddNewListViewController.h
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "List.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddNewListViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *titleTextField;
@property (weak, nonatomic) IBOutlet UICollectionView *iconCollectionView;
@property (nonatomic) List *myList;

@end

NS_ASSUME_NONNULL_END
