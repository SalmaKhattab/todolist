//
//  ItemTableViewCell.h
//  ToDoList
//
//  Created by Salma Khattab on 15/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"

NS_ASSUME_NONNULL_BEGIN

@interface ItemTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *mainLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *reminderImageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *reminderImageView;
@property (weak, nonatomic)  Item *dataSource;

@end

NS_ASSUME_NONNULL_END
