//
//  IconCollectionViewCell.h
//  ToDoList
//
//  Created by Salma Khattab on 18/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IconCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic)  NSMutableDictionary *dataSource;
@property (readwrite) BOOL selectedIndex;

@end

NS_ASSUME_NONNULL_END
