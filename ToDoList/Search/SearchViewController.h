//
//  SearchViewController.h
//  ToDoList
//
//  Created by Salma Khattab on 14/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBarView;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end

