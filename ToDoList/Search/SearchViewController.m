//
//  SearchViewController.m
//  ToDoList
//
//  Created by Salma Khattab on 14/04/2020.
//  Copyright © 2020 Salma Khattab. All rights reserved.
//

#import "SearchViewController.h"
#import "MainTableViewCell.h"
#import "ListDetailedTableViewController.h"
#import "List.h"

@interface SearchViewController ()

@end

@implementation SearchViewController {
    
    RLMResults *filteredTableData;
    RLMResults *myLists;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.title = @"Search";
    [self.listTableView registerNib:[UINib nibWithNibName:@"MainTableViewCell" bundle:nil] forCellReuseIdentifier:@"mainCell"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (filteredTableData.count > 0) {
        
        self.listTableView.backgroundView = nil;
    } else {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        messageLabel.text = @"No Results, Try different keywords.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont systemFontOfSize:20];
        [messageLabel sizeToFit];
        self.listTableView.backgroundView = messageLabel;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return filteredTableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MainTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mainCell" forIndexPath:indexPath];
    if(indexPath.row % 2 == 0)
        cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:250.0/255.0 blue:242.0/255.0 alpha:1.0];
    else
        cell.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    cell.dataSource = filteredTableData[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"showListDetailsFromSearchSegue" sender:filteredTableData[indexPath.row]];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"showListDetailsFromSearchSegue"]) {
        
        ListDetailedTableViewController *vc = segue.destinationViewController;
        vc.myList = sender;
    }
}

#pragma mark - SearchBar Delegate Methods

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    filteredTableData = [List Query:searchText];
    [self.listTableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)SearchBar {
    
  SearchBar.showsCancelButton=YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)theSearchBar {
    
  [theSearchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)SearchBar {
  
    SearchBar.showsCancelButton=NO;
    [SearchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)SearchBar {
    
  [SearchBar resignFirstResponder];
}

@end
